'use strict';

module.exports = function(app) {
  var igdbCtrl = require('../controllers/igdb-controller');

  app.route('/search/:text')
    .get(igdbCtrl.jeuMotcle);

  app.route('/game/:gameid')
    .get(igdbCtrl.nomJeu);

  app.route('/cache')
   .delete(igdbCtrl.suppCache);

};